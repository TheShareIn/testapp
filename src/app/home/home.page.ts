import { Component, ElementRef, ViewChild } from '@angular/core';

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  LatLng,
  MarkerOptions,
  Marker
} from "@ionic-native/google-maps";
import { NavController } from '@ionic/angular';
import { from } from 'rxjs';
import { timeout } from 'q';

declare var google;


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})



export class HomePage {

  map: any;
  markers = [];
  markersStatic = [];


   latLngTehuacan01 = new google.maps.LatLng('18.4638619', '-97.3865127');
   latLngPuebla02 = new google.maps.LatLng('19.0752622', '-98.2056052');
   latLngAtlixco03 = new google.maps.LatLng('19.5554553', '-97.6554148');
   latLngQueretaro04 = new google.maps.LatLng('20.8542575', '-99.84756');
   latLngPachuca05 = new google.maps.LatLng('20.1165413', '-98.7413535');



  directionsService = new google.maps.DirectionsService();


  @ViewChild('map', {static: true}) private element: ElementRef;


  constructor(public googleMaps: GoogleMaps,
              public nav: NavController) {}
    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
      this.initMap();
      this.calculateAndDisplayRoute();
    }

    initMap() {
      const mapOptions = {
          center: this.latLngTehuacan01,
          zoom: 20,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
      this.map = new google.maps.Map(this.element.nativeElement, mapOptions);
      this.addMarkerStatic(this.map, this.latLngTehuacan01);
      this.addMarkerStatic(this.map, this.latLngPuebla02);
      this.addMarkerStatic(this.map, this.latLngAtlixco03);
      this.addMarkerStatic(this.map, this.latLngQueretaro04);
      this.addMarkerStatic(this.map, this.latLngPachuca05);


      this.map.addListener('tilesloaded', () => {
          console.log('accuracy', this.map);
      });
    }
    addMarker(map: any, lat: any, lng: any) {

      const latLng = new google.maps.LatLng(lat, lng);
      const icon = {
        url: '../../assets/icon/bus.png', // image url
        scaledSize: new google.maps.Size(50, 50), // scaled size
      };

      const marker = new google.maps.Marker({
        map,
        animation: google.maps.Animation.BOUNCE,
        position: latLng,
        icon
      });
      this.markers.push(marker);
    }
    addMarkerStatic(map: any, latlng: any) {
      const icon = {
        url: '../../assets/icon/stopBus.png', // image url
        scaledSize: new google.maps.Size(50, 50), // scaled size
      };

      const marker = new google.maps.Marker({
        map,
        animation: google.maps.Animation.BOUNCE,
        position: latlng,
        icon
      });
      this.markersStatic.push(marker);
    }

      calculateAndDisplayRoute() {
        this.directionsService.route({
          origin: this.latLngTehuacan01,
          destination:  this.latLngPachuca05,
          waypoints: [{location: this.latLngPuebla02, stopover: true},
            {location: this.latLngAtlixco03, stopover: true},
            {location: this.latLngQueretaro04, stopover: true}],
          travelMode: 'DRIVING'
        }, (response, status) => {
            if (status === 'OK') {
              let counter = 0;
              const limit = response.routes[0].overview_path.length;
              const interval = setInterval(() => {
                try {
                  const lat = response.routes[0].overview_path[counter].lat();
                  const lng = response.routes[0].overview_path[counter].lng();
                  this.markers.forEach(marker => {
                    marker.setMap(null);
                  });
                  counter++;
                  this.addMarker(this.map, lat, lng);
                  const latLngOrigin = new google.maps.LatLng(lat, lng);
                  this.map.setZoom(15);
                  this.map.setCenter(latLngOrigin);
  
                  if (counter > limit ) {
                    clearInterval(interval);
                  }
                } catch (error) {
                  counter++;
                }

              }, 1500);
            } else {
                console.log('Directions request failed due to ' + status);
            }
          });
        }
}
